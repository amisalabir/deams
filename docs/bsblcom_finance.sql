-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 30, 2018 at 02:55 PM
-- Server version: 10.0.28-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bsblcom_finance`
--

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE `entries` (
  `id` bigint(18) NOT NULL,
  `tag_id` bigint(18) DEFAULT NULL,
  `entrytype_id` bigint(18) NOT NULL,
  `number` bigint(18) DEFAULT NULL,
  `date` date NOT NULL,
  `dr_total` decimal(25,2) NOT NULL DEFAULT '0.00',
  `cr_total` decimal(25,2) NOT NULL DEFAULT '0.00',
  `narration` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `entries`
--

INSERT INTO `entries` (`id`, `tag_id`, `entrytype_id`, `number`, `date`, `dr_total`, `cr_total`, `narration`) VALUES
(1, NULL, 1, 52, '2018-01-01', '545818.00', '545818.00', 'SALE : MV LONG FU STAR\r\nChamber hole plate @ Tk36.40 /kg received vide SBAC cheque 0335347 dt 03-01-2018 for tk 5,45,818 ');

-- --------------------------------------------------------

--
-- Table structure for table `entryitems`
--

CREATE TABLE `entryitems` (
  `id` bigint(18) NOT NULL,
  `entry_id` bigint(18) NOT NULL,
  `ledger_id` bigint(18) NOT NULL,
  `amount` decimal(25,2) NOT NULL DEFAULT '0.00',
  `dc` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `reconciliation_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `entryitems`
--

INSERT INTO `entryitems` (`id`, `entry_id`, `ledger_id`, `amount`, `dc`, `reconciliation_date`) VALUES
(1, 1, 96, '545818.00', 'C', NULL),
(2, 1, 56, '545818.00', 'D', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `entrytypes`
--

CREATE TABLE `entrytypes` (
  `id` bigint(18) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `base_type` int(2) NOT NULL DEFAULT '0',
  `numbering` int(2) NOT NULL DEFAULT '1',
  `prefix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zero_padding` int(2) NOT NULL DEFAULT '0',
  `restriction_bankcash` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `entrytypes`
--

INSERT INTO `entrytypes` (`id`, `label`, `name`, `description`, `base_type`, `numbering`, `prefix`, `suffix`, `zero_padding`, `restriction_bankcash`) VALUES
(1, 'receipt', 'Receipt', 'Received in Bank account or Cash account', 1, 1, 'R-', '', 0, 2),
(2, 'payment', 'Payment', 'Payment made from Bank account or Cash account', 1, 1, 'P-', '', 0, 3),
(3, 'contra', 'Contra', 'Transfer between Bank account and Cash account', 1, 1, '', '', 0, 4),
(4, 'journal', 'Journal', 'Transfer between Non Bank account and Cash account', 1, 1, '', '', 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(18) NOT NULL,
  `parent_id` bigint(18) DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `affects_gross` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `parent_id`, `name`, `code`, `affects_gross`) VALUES
(1, NULL, 'Assets', NULL, 0),
(2, NULL, 'Liabilities and Owners Equity', NULL, 0),
(3, NULL, 'Incomes', NULL, 0),
(4, NULL, 'Expenses', NULL, 0),
(5, 1, 'Fixed Assets', NULL, 0),
(6, 1, 'Current Assets', NULL, 0),
(7, 1, 'Investments', NULL, 0),
(8, 2, 'Capital Account', NULL, 0),
(9, 2, 'Current Liabilities', NULL, 0),
(10, 2, 'Loans (Liabilities)', NULL, 0),
(11, 3, 'Direct Incomes', NULL, 1),
(12, 4, 'Direct Expenses', NULL, 1),
(13, 3, 'Indirect Incomes', NULL, 0),
(14, 4, 'Indirect Expenses', NULL, 0),
(15, 3, 'Sales', NULL, 1),
(16, 4, 'Purchases', NULL, 1),
(17, 15, 'PARTY', NULL, 1),
(18, 8, 'BANK', NULL, 1),
(19, 15, 'SHIP', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ledgers`
--

CREATE TABLE `ledgers` (
  `id` bigint(18) NOT NULL,
  `group_id` bigint(18) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_balance` decimal(25,2) NOT NULL DEFAULT '0.00',
  `op_balance_dc` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(2) NOT NULL DEFAULT '0',
  `reconciliation` int(1) NOT NULL DEFAULT '0',
  `notes` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ledgers`
--

INSERT INTO `ledgers` (`id`, `group_id`, `name`, `code`, `op_balance`, `op_balance_dc`, `type`, `reconciliation`, `notes`) VALUES
(1, 17, 'BASHIR', NULL, '0.00', 'D', 1, 0, ''),
(2, 17, 'BHAI BHAI ENT./HAMUNIA', NULL, '0.00', 'D', 1, 0, ''),
(3, 17, 'BHAI BHAI ENT. / LONG FU', NULL, '0.00', 'D', 1, 0, ''),
(4, 17, 'BISMILLAH TRADERS', NULL, '0.00', 'D', 1, 0, ''),
(5, 17, 'DELWARA', NULL, '0.00', 'D', 1, 0, ''),
(6, 17, 'HOSSAIN', NULL, '0.00', 'D', 1, 0, ''),
(7, 17, 'JAFAR T & L GUDDER ', NULL, '0.00', 'D', 1, 0, ''),
(8, 17, 'JAFAR /1.5-2\" PLATE', NULL, '0.00', 'D', 1, 0, ''),
(9, 17, 'JAFAR / KONAKATA', NULL, '0.00', 'D', 1, 0, ''),
(10, 17, 'JAFAR / BIG CUTPIC', NULL, '0.00', 'D', 1, 0, ''),
(11, 17, 'JOYNAL(1) HESCOVER', NULL, '0.00', 'D', 1, 0, ''),
(12, 17, 'JOYNAL(1) BRASH PROPELLER', NULL, '0.00', 'D', 1, 0, ''),
(13, 17, 'KALU/SELIM', NULL, '0.00', 'D', 1, 0, ''),
(14, 17, 'KAMAL MEMBER', NULL, '0.00', 'D', 1, 0, ''),
(15, 17, 'M/S BISMILLAH ELEC.', NULL, '0.00', 'D', 1, 0, ''),
(16, 17, 'M/S KALI ENT.', NULL, '0.00', 'D', 1, 0, ''),
(17, 17, 'M/S SADIA ENT.', NULL, '0.00', 'D', 1, 0, ''),
(18, 17, 'M/S TIPU ENT.', NULL, '0.00', 'D', 1, 0, ''),
(19, 17, 'MD. ALI/ASHEKIN', NULL, '0.00', 'D', 1, 0, ''),
(20, 17, 'MOHIUDDIN', NULL, '0.00', 'D', 1, 0, ''),
(21, 17, 'ROBIUL', NULL, '0.00', 'D', 1, 0, ''),
(22, 17, 'S.T METAL', NULL, '0.00', 'D', 1, 0, ''),
(23, 17, 'SALAUDDIN', NULL, '0.00', 'D', 1, 0, ''),
(24, 17, 'PENINSULA STEEL LTD', NULL, '0.00', 'D', 1, 0, ''),
(25, 17, 'JASIM / MOZAFFAR ', NULL, '0.00', 'D', 1, 0, ''),
(26, 17, 'SAMIM ', NULL, '0.00', 'D', 1, 0, ''),
(27, 17, 'SAMIM / MAHI ENT', NULL, '0.00', 'D', 1, 0, ''),
(28, 17, 'ST METAL RUBEL', NULL, '0.00', 'D', 1, 0, ''),
(29, 17, 'ALAUDDIN ', 'SANU', '0.00', 'D', 1, 0, ''),
(30, 17, 'KHOKAN ', NULL, '0.00', 'D', 1, 0, ''),
(31, 17, 'LOKMAN', NULL, '0.00', 'D', 1, 0, ''),
(32, 17, 'HEAD OFFICE', NULL, '0.00', 'D', 1, 0, ''),
(33, 17, 'YARD', NULL, '0.00', 'D', 1, 0, ''),
(34, 17, 'JOYNAL(1) SCRAP', NULL, '0.00', 'D', 1, 0, ''),
(35, 17, 'JOYNAL (1) 5/8 PLATE', NULL, '0.00', 'D', 1, 0, ''),
(36, 17, 'NEZAM', NULL, '0.00', 'D', 1, 0, ''),
(37, 17, 'SAKIB ENTERPRISE', NULL, '0.00', 'D', 1, 0, ''),
(38, 17, 'JOYNAL (2)', NULL, '0.00', 'D', 1, 0, ''),
(39, 17, 'FIVE STAR', NULL, '0.00', 'D', 1, 0, ''),
(40, 17, 'RUBEL/MALEK', NULL, '0.00', 'D', 1, 0, ''),
(41, 18, 'Prime CD 1149 /BSBL', NULL, '0.00', 'D', 1, 0, 'PRIME BANK LTD.'),
(42, 18, 'Prime CD 9653 / BSL', NULL, '0.00', 'D', 1, 0, 'PRIME BANK LTD.'),
(43, 18, 'Prime CD 0624 / BSML', NULL, '0.00', 'D', 1, 0, 'PRIME BANK LTD.'),
(44, 18, 'Prime CD 0004 / BSML', NULL, '0.00', 'D', 1, 0, 'PRIME BANK LTD.'),
(45, 18, 'Jamuna CD 7076 / BSBL', NULL, '0.00', 'D', 1, 0, 'JAMUNA BANK LTD.'),
(46, 18, 'Jamuna CD 13289 / BSBL', NULL, '0.00', 'D', 1, 0, 'JAMUNA BANK LTD.'),
(47, 18, 'Jamuna CC 1065 / BSL', NULL, '0.00', 'D', 1, 0, 'JAMUNA BANK LTD.'),
(48, 18, 'Jamuna CD 5594 / BSL', NULL, '0.00', 'D', 1, 0, 'JAMUNA BANK LTD.'),
(49, 18, 'Jamuna CD 6877 / BSML', NULL, '0.00', 'D', 1, 0, 'JAMUNA BANK LTD.'),
(50, 18, 'Asia OD 0005 / BSBL', NULL, '0.00', 'D', 1, 0, 'BANK ASIA'),
(51, 18, 'Asia STD 0080 / BSBL', NULL, '0.00', 'D', 1, 0, 'BANK ASIA'),
(52, 18, 'Asia CD 1137 / BSL', NULL, '0.00', 'D', 1, 0, 'BANK ASIA'),
(53, 18, 'AB CD 7000 / BSBL', NULL, '0.00', 'D', 1, 0, 'AB BANK LTD.'),
(54, 18, 'AB CD 8000 / BSL', NULL, '0.00', 'D', 1, 0, 'AB BANK LTD.'),
(55, 18, 'Dhaka CD 6843 / BSBL', NULL, '0.00', 'D', 1, 0, 'DHAKA BANK LTD.'),
(56, 18, 'SBAC CD 3936 / BSBL', NULL, '0.00', 'D', 1, 0, 'SOUTH BANGLA AGRICULTURE AND COMMERCE BANK LTD.'),
(57, 18, 'SBAC CC 029 / Banani', NULL, '0.00', 'D', 0, 0, 'SOUTH BANGLA AGRICULTURE AND COMMERCE BANK LTD.'),
(58, 18, 'ONE CD 3451 / BSL', NULL, '0.00', 'D', 1, 0, 'ONE BANK LTD.'),
(59, 18, 'ONE CD 9001 BSBL', NULL, '0.00', 'D', 1, 0, 'ONE BANK LTD.'),
(60, 18, 'NBL CD 0225 / BSBL', NULL, '0.00', 'D', 1, 0, 'NATIONAL BANK LTD'),
(61, 18, 'Marcantile CC 0053', NULL, '0.00', 'D', 1, 0, 'MARCANTILE BANK LTD.'),
(62, 18, 'UCB CD 3696 / BSL', NULL, '0.00', 'D', 1, 0, 'UNITED COMMERCIAL BANK LTD.'),
(63, 13, 'ADVANCE SALARY', NULL, '0.00', 'C', 1, 0, ''),
(64, 12, 'AUDIT FEE', NULL, '0.00', 'D', 1, 0, ''),
(65, 12, 'BANANI TRADERS', NULL, '0.00', 'D', 1, 0, ''),
(66, 14, 'BANK CHARGES', NULL, '0.00', 'D', 1, 0, ''),
(67, 19, 'MV POSIDON 	', NULL, '0.00', 'C', 1, 0, ''),
(68, 19, 'MV RITALINK', NULL, '0.00', 'C', 1, 0, ''),
(69, 19, 'MV SANTA ELENA', NULL, '0.00', 'C', 1, 0, ''),
(70, 19, 'MV KONA', NULL, '0.00', 'C', 1, 0, ''),
(71, 19, 'MV TOLEDO', NULL, '0.00', 'C', 1, 0, ''),
(72, 19, 'MV SYDPOLINE', NULL, '0.00', 'C', 1, 0, ''),
(73, 19, 'MV HERETTE', NULL, '0.00', 'C', 1, 0, 'MV HERETTE'),
(74, 19, 'MV DOLPHINA', NULL, '0.00', 'C', 1, 0, 'MV DOLPHINA'),
(75, 19, 'MV SALIMA-S', NULL, '0.00', 'C', 1, 0, ''),
(76, 19, 'MV ELENA-B', NULL, '0.00', 'C', 1, 0, ''),
(77, 19, 'MV MERINA BAY', NULL, '0.00', 'C', 1, 0, 'MV MERINA BAY'),
(78, 19, 'MV CS CRISTINE', NULL, '0.00', 'C', 1, 0, ''),
(79, 19, 'MV OCEN ALFA', NULL, '0.00', 'C', 1, 0, ''),
(80, 19, 'MV HOUMA BELEE', NULL, '0.00', 'D', 1, 0, ''),
(81, 19, 'MV FRANCISCO', NULL, '0.00', 'C', 1, 0, ''),
(82, 19, 'MV ICE', NULL, '0.00', 'C', 1, 0, ''),
(83, 19, 'MV KAI', NULL, '0.00', 'C', 1, 0, ''),
(84, 19, 'MV CASTILLO', NULL, '0.00', 'C', 1, 0, ''),
(85, 19, 'MV ARON', NULL, '0.00', 'C', 1, 0, ''),
(86, 19, 'MV POLASKA', NULL, '0.00', 'C', 1, 0, ''),
(87, 19, 'MV CARAKA', NULL, '0.00', 'C', 1, 0, ''),
(88, 19, 'MV ORIENTAL', NULL, '0.00', 'C', 1, 0, ''),
(89, 19, 'MV RESOLVE', NULL, '0.00', 'C', 1, 0, ''),
(90, 19, 'MV ACER', NULL, '0.00', 'C', 1, 0, ''),
(91, 19, 'MV KOREA GAS', NULL, '0.00', 'C', 1, 0, ''),
(92, 19, 'MV HUITAI', NULL, '0.00', 'C', 1, 0, ''),
(93, 19, 'MT GAS MASTER', NULL, '0.00', 'C', 1, 0, ''),
(94, 19, 'MV HAMMONIA', NULL, '0.00', 'C', 1, 0, ''),
(95, 19, 'MV STAR MONISHA', NULL, '0.00', 'C', 1, 0, ''),
(96, 19, 'MV LONG FU STAR', NULL, '0.00', 'C', 1, 0, ''),
(97, 19, 'MV POS CHALLENGER', NULL, '0.00', 'C', 1, 0, ''),
(98, 19, 'MV LAKE HILL 7', NULL, '0.00', 'C', 1, 0, ''),
(99, 19, 'MV MANDRAKI', NULL, '0.00', 'C', 1, 0, ''),
(100, 19, 'BSBL & VAS', NULL, '0.00', 'C', 1, 0, ''),
(101, 19, 'MV LPG DOLPHINE', NULL, '0.00', 'C', 1, 0, ''),
(102, 19, 'MV KINA', NULL, '0.00', 'C', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` bigint(18) NOT NULL,
  `date` datetime NOT NULL,
  `level` int(1) NOT NULL,
  `host_ip` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `date`, `level`, `host_ip`, `user`, `url`, `user_agent`, `message`) VALUES
(1, '2018-04-30 07:47:39', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/groups/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Group : PARTY'),
(2, '2018-04-30 07:47:59', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/groups/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Group : BANK'),
(3, '2018-04-30 08:13:11', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/entrytypes/edit/1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Edited Entrytype : Receipt'),
(4, '2018-04-30 08:13:27', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/entrytypes/edit/2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Edited Entrytype : Payment'),
(5, '2018-04-30 08:18:48', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : BASHIR'),
(6, '2018-04-30 08:19:48', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : BHAI BHAI ENT./HAMUNIA'),
(7, '2018-04-30 08:20:07', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : BHAI BHAI ENT. / LONG FU'),
(8, '2018-04-30 08:20:30', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/edit/2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Edited Ledger : BHAI BHAI ENT./HAMUNIA'),
(9, '2018-04-30 08:20:48', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : BISMILLAH TRADERS'),
(10, '2018-04-30 08:21:08', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : DELWARA'),
(11, '2018-04-30 08:21:26', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : HOSSAIN'),
(12, '2018-04-30 08:21:49', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JAFAR T & L GUDDER '),
(13, '2018-04-30 08:22:05', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JAFAR /1.5-2\" PLATE'),
(14, '2018-04-30 08:22:33', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JAFAR / KONAKATA'),
(15, '2018-04-30 08:22:46', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JAFAR / BIG CUTPIC'),
(16, '2018-04-30 08:23:08', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JOYNAL(1) HESCOVER'),
(17, '2018-04-30 08:23:45', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JOYNAL(1) BRASH PROPELLER'),
(18, '2018-04-30 08:24:02', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : KALU/SELIM'),
(19, '2018-04-30 08:24:21', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : KAMAL MEMBER'),
(20, '2018-04-30 08:24:36', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : M/S BISMILLAH ELEC.'),
(21, '2018-04-30 08:24:54', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : M/S KALI ENT.'),
(22, '2018-04-30 08:25:17', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : M/S SADIA ENT.'),
(23, '2018-04-30 08:25:38', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : M/S TIPU ENT.'),
(24, '2018-04-30 08:26:14', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MD. ALI/ASHEKIN'),
(25, '2018-04-30 08:27:57', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MOHIUDDIN'),
(26, '2018-04-30 08:28:14', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : ROBIUL'),
(27, '2018-04-30 08:28:35', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : S.T METAL'),
(28, '2018-04-30 08:28:56', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : SALAUDDIN'),
(29, '2018-04-30 08:29:11', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : PENINSULA STEEL LTD'),
(30, '2018-04-30 08:29:27', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JASIM / MOZAFFAR '),
(31, '2018-04-30 08:29:40', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : SAMIM '),
(32, '2018-04-30 08:29:57', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : SAMIM / MAHI ENT'),
(33, '2018-04-30 08:30:12', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : ST METAL RUBEL'),
(34, '2018-04-30 08:30:41', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : ALAUDDIN '),
(35, '2018-04-30 08:30:59', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : KHOKAN '),
(36, '2018-04-30 08:31:15', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : LOKMAN'),
(37, '2018-04-30 08:31:33', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : HEAD OFFICE'),
(38, '2018-04-30 08:31:47', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : YARD'),
(39, '2018-04-30 08:32:01', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JOYNAL(1) SCRAP'),
(40, '2018-04-30 08:32:15', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JOYNAL (1) 5/8 PLATE'),
(41, '2018-04-30 08:32:39', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : NEZAM'),
(42, '2018-04-30 08:32:51', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : SAKIB ENTERPRISE'),
(43, '2018-04-30 08:33:04', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : JOYNAL (2)'),
(44, '2018-04-30 08:33:17', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : FIVE STAR'),
(45, '2018-04-30 08:33:39', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : RUBEL/MALEK'),
(46, '2018-04-30 09:13:57', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Prime CD 1149 /BSBL'),
(47, '2018-04-30 09:14:57', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Prime CD 9653 / BSL'),
(48, '2018-04-30 09:15:37', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Prime CD 0624 / BSML'),
(49, '2018-04-30 09:16:06', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Prime CD 0004 / BSML'),
(50, '2018-04-30 09:16:46', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Jamuna CD 7076 / BSBL'),
(51, '2018-04-30 09:17:16', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Jamuna CD 13289 / BSBL'),
(52, '2018-04-30 09:17:45', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Jamuna CC 1065 / BSL'),
(53, '2018-04-30 09:18:08', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Jamuna CD 5594 / BSL'),
(54, '2018-04-30 09:18:32', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Jamuna CD 6877 / BSML'),
(55, '2018-04-30 09:19:01', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Asia OD 0005 / BSBL'),
(56, '2018-04-30 09:19:30', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Asia STD 0080 / BSBL'),
(57, '2018-04-30 09:21:03', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Asia CD 1137 / BSL'),
(58, '2018-04-30 09:21:30', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : AB CD 7000 / BSBL'),
(59, '2018-04-30 09:21:54', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : AB CD 8000 / BSL'),
(60, '2018-04-30 09:22:21', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Dhaka CD 6843 / BSBL'),
(61, '2018-04-30 09:22:45', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : SBAC CD 3936 / BSBL'),
(62, '2018-04-30 09:24:17', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : SBAC CC 029 / Banani'),
(63, '2018-04-30 09:24:55', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : ONE CD 3451 / BSL'),
(64, '2018-04-30 09:25:21', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : ONE CD 9001 BSBL'),
(65, '2018-04-30 09:25:46', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : NBL CD 0225 / BSBL'),
(66, '2018-04-30 09:26:09', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : Marcantile CC 0053'),
(67, '2018-04-30 09:26:33', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : UCB CD 3696 / BSL'),
(68, '2018-04-30 09:45:13', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : ADVANCE SALARY'),
(69, '2018-04-30 09:45:40', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : AUDIT FEE'),
(70, '2018-04-30 09:46:17', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : BANANI TRADERS'),
(71, '2018-04-30 09:46:55', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : BANK CHARGES'),
(72, '2018-04-30 10:22:28', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/groups/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Group : SHIP'),
(73, '2018-04-30 10:23:26', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV POSIDON 	'),
(74, '2018-04-30 10:23:59', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV RITALINK'),
(75, '2018-04-30 10:24:24', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV SANTA ELENA'),
(76, '2018-04-30 10:24:45', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV KONA'),
(77, '2018-04-30 10:25:19', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV TOLEDO'),
(78, '2018-04-30 10:26:49', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV SYDPOLINE'),
(79, '2018-04-30 10:27:07', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV HERETTE'),
(80, '2018-04-30 10:27:28', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV DOLPHINA'),
(81, '2018-04-30 10:27:48', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV SALIMA-S'),
(82, '2018-04-30 10:28:04', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV ELENA-B'),
(83, '2018-04-30 10:28:25', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV MERINA BAY'),
(84, '2018-04-30 10:28:46', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/edit/75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Edited Ledger : MV SALIMA-S'),
(85, '2018-04-30 10:29:24', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV CS CRISTINE'),
(86, '2018-04-30 10:29:42', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV OCEN ALFA'),
(87, '2018-04-30 10:31:17', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV HOUMA BELEE'),
(88, '2018-04-30 10:31:38', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV FRANCISCO'),
(89, '2018-04-30 10:32:31', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV ICE'),
(90, '2018-04-30 10:32:51', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV KAI'),
(91, '2018-04-30 10:33:35', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV CASTILLO'),
(92, '2018-04-30 10:34:45', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV ARON'),
(93, '2018-04-30 10:35:07', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV POLASKA'),
(94, '2018-04-30 10:35:30', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV CARAKA'),
(95, '2018-04-30 10:35:48', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV ORIENTAL'),
(96, '2018-04-30 10:36:04', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV RESOLVE'),
(97, '2018-04-30 10:42:05', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV ACER'),
(98, '2018-04-30 10:42:27', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV KOREA GAS'),
(99, '2018-04-30 10:42:58', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV HUITAI'),
(100, '2018-04-30 10:43:33', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MT GAS MASTER'),
(101, '2018-04-30 10:44:06', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV HAMMONIA'),
(102, '2018-04-30 10:44:19', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV STAR MONISHA'),
(103, '2018-04-30 10:44:42', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV LONG FU STAR'),
(104, '2018-04-30 10:45:04', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV POS CHALLENGER'),
(105, '2018-04-30 10:45:20', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV LAKE HILL 7'),
(106, '2018-04-30 10:45:39', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV MANDRAKI'),
(107, '2018-04-30 10:45:58', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : BSBL & VAS'),
(108, '2018-04-30 10:46:21', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV LPG DOLPHINE'),
(109, '2018-04-30 10:46:39', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/ledgers/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Ledger : MV KINA'),
(110, '2018-04-30 10:51:10', 1, '103.103.239.36', 'bsbl', 'http://104.152.168.32/~bsblcom/bsbl/entries/add/receipt', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 'Added Receipt entry numbered R-52');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fy_start` date NOT NULL,
  `fy_end` date NOT NULL,
  `currency_symbol` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `currency_format` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `decimal_places` int(2) NOT NULL DEFAULT '2',
  `date_format` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `manage_inventory` int(1) NOT NULL DEFAULT '0',
  `account_locked` int(1) NOT NULL DEFAULT '0',
  `email_use_default` int(1) NOT NULL DEFAULT '0',
  `email_protocol` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `email_host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_port` int(5) NOT NULL,
  `email_tls` int(1) NOT NULL DEFAULT '0',
  `email_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `print_paper_height` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_paper_width` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_margin_top` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_margin_bottom` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_margin_left` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_margin_right` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_orientation` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `print_page_format` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `database_version` int(10) NOT NULL,
  `settings` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `address`, `email`, `fy_start`, `fy_end`, `currency_symbol`, `currency_format`, `decimal_places`, `date_format`, `timezone`, `manage_inventory`, `account_locked`, `email_use_default`, `email_protocol`, `email_host`, `email_port`, `email_tls`, `email_username`, `email_password`, `email_from`, `print_paper_height`, `print_paper_width`, `print_margin_top`, `print_margin_bottom`, `print_margin_left`, `print_margin_right`, `print_orientation`, `print_page_format`, `database_version`, `settings`) VALUES
(1, 'Bhatiyari Ship Breakers Ltd.', 'Bhatiyari, Chittagong', 'bsbl@olineit.com', '2018-01-01', '2018-12-31', 'Tk.', 'none', 2, 'd-M-Y|dd-M-yy', 'UTC', 0, 0, 1, 'Smtp', '', 0, 0, '', '', '', '0.000', '0.000', '0.000', '0.000', '0.000', '0.000', 'P', 'H', 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(18) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` char(6) COLLATE utf8_unicode_ci NOT NULL,
  `background` char(6) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `entries`
--
ALTER TABLE `entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `entrytype_id` (`entrytype_id`);

--
-- Indexes for table `entryitems`
--
ALTER TABLE `entryitems`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `ledger_id` (`ledger_id`);

--
-- Indexes for table `entrytypes`
--
ALTER TABLE `entrytypes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `label` (`label`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `ledgers`
--
ALTER TABLE `ledgers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entries`
--
ALTER TABLE `entries`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `entryitems`
--
ALTER TABLE `entryitems`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `entrytypes`
--
ALTER TABLE `entrytypes`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `ledgers`
--
ALTER TABLE `ledgers`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `entries`
--
ALTER TABLE `entries`
  ADD CONSTRAINT `entries_fk_check_entrytype_id` FOREIGN KEY (`entrytype_id`) REFERENCES `entrytypes` (`id`),
  ADD CONSTRAINT `entries_fk_check_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);

--
-- Constraints for table `entryitems`
--
ALTER TABLE `entryitems`
  ADD CONSTRAINT `entryitems_fk_check_entry_id` FOREIGN KEY (`entry_id`) REFERENCES `entries` (`id`),
  ADD CONSTRAINT `entryitems_fk_check_ledger_id` FOREIGN KEY (`ledger_id`) REFERENCES `ledgers` (`id`);

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_fk_check_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `groups` (`id`);

--
-- Constraints for table `ledgers`
--
ALTER TABLE `ledgers`
  ADD CONSTRAINT `ledgers_fk_check_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
